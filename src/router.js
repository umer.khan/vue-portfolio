import Vue from 'vue'
import Router from 'vue-router'
import About from './views/About'
import Portfolio from './views/Portfolio.vue'
//import Skillview from './views/Skillview'
import Contact from './views/Contact'
import NewSkill from './components/NewSkill'
import Message from './views/Message'
import EditSkill from './components/EditSkill'
import Skills from './components/Skills'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'about',
      component: About
    },
    {
      path: '/portfolio',
      name: 'portfolio',
      component: Portfolio
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
      {
          path: '/newskill',
          name: 'newskill',
          component: NewSkill
      },
      {
          path: '/message',
          name: 'message',
          component: Message
      },
      {
          path: '/editskill/:skill_id',
          name: 'editskill',
          component: EditSkill
      },
      {
          path: '/skill',
          name: 'skill',
          component: Skills
      }
  ]
})
