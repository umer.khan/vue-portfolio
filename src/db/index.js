import firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
    apiKey: 'AIzaSyDpnvptXOIc-Eyp1kf1uq2Hee9EsPgKyVE',
    authDomain: 'vue-portfolio-53f89.firebaseapp.com',
    databaseURL: 'https://vue-portfolio-53f89.firebaseio.com',
    projectId: 'vue-portfolio-53f89',
    storageBucket: 'vue-portfolio-53f89.appspot.com',
    messagingSenderId: '253589143018'
}

const firebaseapp = firebase.initializeApp(config)

export default firebaseapp.firestore()